// soal 1
var nomor = 20;
var angka = 2;

// jawaban soal 1
console.log("LOOPING PERTAMA");
while (angka <= 20) {
  console.log(angka + " - I love Coding");
  angka += 2;
}
console.log("LOOPING KEDUA");
while (nomor > 0) {
  console.log(nomor + " - I will become a frontend developer");
  nomor -= 2;
}

// jawaban soal 2
for (var angka = 1; angka <= 20; angka++) {
  if (angka % 3 == 0 && angka % 2 == 1) {
    console.log(angka + " - I Love Coding");
  } else if (angka % 2 == 0) {
    console.log(angka + " - Berkualitas");
  } else if (angka % 2 == 1) {
    console.log(angka + " - Santai");
  }
}

// jawaban soal 3
var x, y, chr;
var chr = "#";
for (x = 1; x <= 6; x++) {
  for (y = 1; y < x; y++) {
    chr = chr + "#";
  }
  console.log(chr);
  chr = "";
}

// soal 4
var kalimat = "saya sangat senang belajar javascript";

// jawaban soal 4
var potongan = kalimat.split(" ");

console.log(potongan);

// soal 5
var daftarBuah = [
  "2. Apel",
  "5. Jeruk",
  "3. Anggur",
  "4. Semangka",
  "1. Mangga",
];

// jawaban soal 5
daftarBuah.sort();
var c = daftarBuah.join("\n");
console.log(c);
